package com.example.instagram;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.instagram.Fragments.AccountFragment;
import com.example.instagram.Fragments.AddPostFragment;
import com.example.instagram.Fragments.HomeFragment;
import com.example.instagram.Fragments.MainFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

    private Toolbar mainToolbar;
    private FloatingActionButton addPostBtn;
    private BottomNavigationView mainBottomNav;
    private NavController navController;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth mAuth;
    private String currentUserId;

    private HomeFragment homeFragment;
    private AddPostFragment addPostFragment;
    private AccountFragment accountFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        Picasso.get().setIndicatorsEnabled(false);

        mAuth = FirebaseAuth.getInstance();
        mainBottomNav = (BottomNavigationView)findViewById(R.id.mainBtmNav);

        getSupportActionBar().setTitle("Instagram");

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        mainBottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_action:
                        navController.popBackStack(R.id.homeFragment,false);
                        navController.navigate(R.id.action_homeFragment_self);
                        return true;

                    case R.id.add_action:
                        navController.popBackStack(R.id.homeFragment,false);
                        navController.navigate(R.id.action_homeFragment2_to_addPostFragment);
                        return true;

                    case R.id.account_action:
                        navController.popBackStack(R.id.homeFragment,false);
                        navController.navigate(R.id.action_homeFragment_to_accountFragment);
                        return true;

                        default:
                            return false;

                }

            }
        });

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            // If logout was selected than logout
            case R.id.action_logout_btn:
                logOut();
                return true;

            case R.id.action_settings_btn:
                if (navController.getCurrentDestination().getId() == R.id.homeFragment){
                    navController.navigate(R.id.action_homeFragment_to_setupActivityFragment);
                }else if(navController.getCurrentDestination().getId() == R.id.accountFragment){
                    navController.navigate(R.id.action_accountFragment_to_setupActivityFragment);
                }

                return true;

                default:
                    return false;
        }
        
    }

    private void logOut() {
        mAuth.signOut();

        navController.popBackStack(R.id.mainFragment,false);
        navController.navigate(R.id.action_mainFragment_to_loginFragment);

    }

}
