package com.example.instagram.Model.Daos;

import android.icu.lang.UScript;

import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.Entities.User;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UsersDao {

    @Query("SELECT * From Users")
    List<User> getAllUsers();

    @Query("SELECT * From Users WHERE userId =:userId")
    User getUserById(String userId);

    @Insert(onConflict = REPLACE)
    void insertUser(User user);

    @Insert(onConflict = REPLACE)
    void insertAllUsers(User... users);

    @Query("DELETE FROM users")
    void deleteAllUsers();

    @Query("DELETE FROM Posts WHERE userId = :userId")
    void deleteUser(String userId);
}
