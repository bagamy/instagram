package com.example.instagram.Model.firebase;

import android.util.Log;

import com.example.instagram.Model.Entities.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import androidx.annotation.NonNull;

public class UserFirebase {
    private static UserFirebase instance = null;

    protected UserFirebase() {
        // Exists only to defeat instantiation.
    }

    public static UserFirebase getInstance() {
        if (instance == null) {
            instance = new UserFirebase();
        }
        return instance;
    }

    public static void getUserByUid(String uid, final OnSuccessListener<User> listener) {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        usersRef.orderByChild("userId").equalTo(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //User user = dataSnapshot.getValue(User.class);
                 User user = dataSnapshot.child(uid).getValue(User.class);

                listener.onSuccess(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                listener.onSuccess(null);
            }
        });

    }

    public static void saveUser(User user, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference();
        userRef.child("Users").child(user.getUserId()).setValue(user).addOnSuccessListener(onSuccessListener)
                .addOnFailureListener((error) -> {
                    onFailureListener.onFailure(error);
                });
    }



    public void updateUser(String UID,
                           String userName,
                           String newImage,
                           OnSuccessListener onSuccessListener,
                           OnFailureListener onFailureListener) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> updateVal = new HashMap<>();
        updateVal.put("name", userName);
        updateVal.put("image",newImage);

        userRef.child("Users").child(UID).updateChildren(updateVal)
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);

    }
}
