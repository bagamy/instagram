package com.example.instagram.Model.firebase;

import android.net.Uri;
import android.util.Log;
import android.util.Pair;

import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.Entities.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;

public class PostFirebase {
    private static PostFirebase instance = null;
    private static final String TAG = "PostFirebase";


    protected PostFirebase() {
    }
    ValueEventListener allCollectionseventListener;
    ValueEventListener profileEventListener;

    public static PostFirebase getInstance(){
        if(instance==null){
            instance = new PostFirebase();
        }

        return instance;
    }

    public void saveImage(Uri image,String imageId, final OnSuccessListener<Uri> listener, OnFailureListener onFailureListener) {
        FirebaseStorage storage = FirebaseStorage.getInstance();

        final StorageReference imageRef = storage.getReference().child("images").child(imageId);


        UploadTask uploadTask = imageRef.putFile(image);
        uploadTask.continueWithTask((task) -> {
            if (!task.isSuccessful()) {
                Exception e = task.getException();
                Log.e(TAG, e.toString());
                onFailureListener.onFailure(e);
            }
            return imageRef.getDownloadUrl();
        }).addOnSuccessListener((task) -> {
            listener.onSuccess(task);
        })
                .addOnFailureListener(onFailureListener);
    }

    public static void savePost(Post post, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        DatabaseReference mdatabase = FirebaseDatabase.getInstance().getReference();
        mdatabase.child("Posts").child(post.getPostId()).setValue(post).addOnSuccessListener(onSuccessListener)
                .addOnFailureListener((error) -> {
                    Log.d(TAG, error.toString());
                    onFailureListener.onFailure(error);
                });

    }

    public void updatePost(String postId,
                             String newDesc,
                             OnSuccessListener onSuccessListener,
                             OnFailureListener onFailureListener) {
        DatabaseReference postRef = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> updateVal = new HashMap<>();
        updateVal.put("description", newDesc);
        postRef.child("Posts").child(postId).updateChildren(updateVal)
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);

    }

    public void likePost(String postId, String userId,
    OnSuccessListener onSuccessListener,
    OnFailureListener onFailureListener) {
        DatabaseReference postRef = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> updateVal = new HashMap<>();
        updateVal.put("description", 1);

        postRef.child("Posts").child(postId).child("Likes").updateChildren(updateVal)
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);


    }

    public void dislikePost(String postId, String userId,
                         OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener) {
        DatabaseReference postRef = FirebaseDatabase.getInstance().getReference();
        FirebaseDatabase.getInstance().getReference()
                .child("Posts").child(postId).child("Likes").child(userId).setValue(null)
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);
    }

    public void islikePost(String postId, String userId,
                           final OnSuccessListener<String> listener) {


        DatabaseReference postRef = FirebaseDatabase.getInstance().getReference()
                .child("Posts").child(postId).child("Likes");

        postRef.orderByChild("userId").equalTo(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //User user = dataSnapshot.getValue(User.class);
                if(dataSnapshot.getValue() != null){
                    listener.onSuccess(userId);
                }else{
                    listener.onSuccess(null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               listener.onSuccess(null);
            }
        });
    }
    public void deleteImage(String imageId, OnSuccessListener onSuccessListener, OnFailureListener onFailureListener) {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("images").child(imageId);

        storageReference.delete()
                .addOnSuccessListener(onSuccessListener)
                .addOnFailureListener(onFailureListener);
    }

    public void getAllCollections(final OnSuccessListener listener) {
        DatabaseReference fbPostsRef = FirebaseDatabase.getInstance().getReference();
        if (allCollectionseventListener == null) {
            allCollectionseventListener = fbPostsRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    DataSnapshot postsRef = dataSnapshot.child("Posts");
                    DataSnapshot usersRef = dataSnapshot.child("Users");
                    List<User> users = new ArrayList<>();
                    List<Post> posts = new ArrayList<>();

                    for (DataSnapshot postSnapshot : postsRef.getChildren()) {
                        Post p = postSnapshot.getValue(Post.class);
                        posts.add(p);
                    }

                    for (DataSnapshot postSnapshot : usersRef.getChildren()) {
                        User u = postSnapshot.getValue(User.class);
                        users.add(u);
                    }

                    listener.onSuccess(new Pair(posts, users));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSuccess(null);
                }
            });
        }
    }

    public void getAllCollectionsForProfile(String userUid, final OnSuccessListener listener) {

//        CollectionReference postRef = FirebaseFirestore.getInstance().collection("Posts");
//        CollectionReference userRef = FirebaseFirestore.getInstance().collection("Users");
//        if(profileEventListener == null){
//            profileEventListener = postRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
//                @Override
//                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
//                    DataSnapshot postsRef = queryDocumentSnapshots.
//                    DataSnapshot usersRef = dataSnapshot.child("Users");
//                    List<User> users = new ArrayList<>();
//                    List<Post> posts = new ArrayList<>();
//                }
//            });
//        }
        DatabaseReference fbPostsRef = FirebaseDatabase.getInstance().getReference();
        if (profileEventListener == null) {
            profileEventListener = fbPostsRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    DataSnapshot postsRef = dataSnapshot.child("Posts");
                    DataSnapshot usersRef = dataSnapshot.child("Users");
                    List<User> users = new ArrayList<>();
                    List<Post> posts = new ArrayList<>();

                    for (DataSnapshot postSnapshot : postsRef.getChildren()) {
                        Post p = postSnapshot.getValue(Post.class);
                        posts.add(p);
                    }

                    for (DataSnapshot postSnapshot : usersRef.getChildren()) {
                        User u = postSnapshot.getValue(User.class);
                        users.add(u);
                    }

                    posts = posts.stream().filter(post -> post.getUserId().equals(userUid)).collect(Collectors.toList());
                    User user = users.stream().filter(currUser -> currUser.getUserId().equals(userUid)).findFirst().get();

                    listener.onSuccess(new Pair(posts, user));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    listener.onSuccess(null);
                }
            });
        }
    }

    public void deletePost(String postUid) {
        // delete post image
        // delete post itself
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference imagesRef = storage.getReference().child("images").child(postUid);
        imagesRef.delete();
        FirebaseDatabase.getInstance().getReference()
                .child("Posts").child(postUid).setValue(null);

    }
}
