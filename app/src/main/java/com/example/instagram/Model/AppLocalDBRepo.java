package com.example.instagram.Model;

import com.example.instagram.Model.Daos.PostDao;
import com.example.instagram.Model.Daos.UsersDao;
import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.Entities.User;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {User.class, Post.class}, version = 1,exportSchema=false)
public abstract class AppLocalDBRepo extends RoomDatabase {
    public abstract UsersDao usersDao();
    public abstract PostDao postDao();

}
