package com.example.instagram.Model.Entities;

import com.google.firebase.database.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Users")
public class User {

    @PrimaryKey
    @NonNull
    private String userId;
    //private String imageId;



    private String name;
    private String image;


    public User(){

    }
    public User(@NonNull String userId, String name, String image, String imageId) {
        this.userId = userId;
        this.name = name;
        this.image = image;
        //this.imageId = imageId;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NonNull String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

//    public String getImageId() {
//        return imageId;
//    }
//
//    public void setImageId(String imageId) {
//        this.imageId = imageId;
//    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


}
