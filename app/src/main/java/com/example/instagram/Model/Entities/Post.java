package com.example.instagram.Model.Entities;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Posts")
public class Post {


    @PrimaryKey
    @NonNull
    private String postId;
    private String description;
    private String userId;
    private String image_uri;
    private Long timestamp;

//    public Post(){
//
//    }
//
//    public Post(String description, String userId, String imageURI, Long timestamp) {
//        this.description = description;
//        this.userId = userId;
//        this.image_uri = imageURI;
//        this.timestamp = timestamp;
//    }


    @NonNull
    public String getPostId() {
        return postId;
    }

    public void setPostId(@NonNull String postId) {
        this.postId = postId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImage_uri() {
        return image_uri;
    }

    public void setImage_uri(String image_uri) {
        this.image_uri = image_uri;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

}
