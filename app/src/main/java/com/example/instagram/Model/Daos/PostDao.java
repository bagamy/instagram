package com.example.instagram.Model.Daos;






import com.example.instagram.Model.Entities.Post;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface PostDao {

    @Query("SELECT * From Posts ORDER BY timestamp DESC")
    List<Post> getAllPosts();

    @Query("SELECT * From posts WHERE postId =:postId ORDER BY timestamp DESC")
    Post getPostById(String postId);

    @Insert(onConflict = REPLACE)
    void insertPost(Post post);

    @Insert(onConflict = REPLACE)
    void insertAllPosts(Post... posts);

    @Query("DELETE FROM Posts")
    void deleteAllPosts();

    @Query("DELETE FROM Posts WHERE postId = :postId")
    void deletePost(String postId);
}
