package com.example.instagram.Model.UI;

import com.squareup.picasso.RequestCreator;

import java.util.Date;

public class UIPost {
    public String postId;
    public String description;
    public String userName;

    public String getUserId() {
        return userId;
    }

    public String userId;
    public Long timestamp;
    public RequestCreator postImageRequestCreator;
    public String imageUri;
    public RequestCreator userProfileRequestCreator;

    public Long getTimestamp() {
        return timestamp;
    }
}