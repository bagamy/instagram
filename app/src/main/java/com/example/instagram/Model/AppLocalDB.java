package com.example.instagram.Model;

import com.example.instagram.Instagram;


import androidx.room.Room;

public class AppLocalDB{
        static public AppLocalDBRepo db =
                Room.databaseBuilder(Instagram.context,
                        AppLocalDBRepo.class, "instagramDb.db")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration().build();

}
