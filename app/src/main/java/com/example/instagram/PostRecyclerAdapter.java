package com.example.instagram;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import com.example.instagram.Model.UI.UIPost;
import com.example.instagram.Model.firebase.PostFirebase;
import com.squareup.picasso.Callback;


public class PostRecyclerAdapter extends RecyclerView.Adapter<PostRecyclerAdapter.PostHolder> {

    private Context mContext;
    public List<UIPost> Posts;
    public static OnItemClickListener onItemClickListener;
    private String userId;


    public PostRecyclerAdapter(List<UIPost> posts) {
        if (posts == null) {
            Posts = new LinkedList<>();
        } else {
            Posts = posts;
        }
        onItemClickListener = null;
    }

    public PostRecyclerAdapter(Context context, List<UIPost> posts,String userId ,OnItemClickListener onItemClickListener) {
        this.mContext = context;
        this.Posts = posts;
        this.userId = userId;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);

        PostHolder ph = new PostHolder(v);

        return ph;
    }

    @Override
    public void onBindViewHolder(@NonNull PostHolder holder, int position) {
        UIPost currentPost = this.Posts.get(position);
        holder.postUid = currentPost.postId;
        currentPost.userProfileRequestCreator.into(holder.postUserImage);
        currentPost.postImageRequestCreator.into(holder.postImageView);
        holder.postDescView.setText(currentPost.description);
        holder.descUsername.setText(currentPost.userName);
        DateFormat df = new SimpleDateFormat("MMMM dd, yyyy");
        holder.postDateView.setText(df.format(currentPost.timestamp));
        holder.postUsernameView.setText(currentPost.userName);
        if(currentPost.userId.equals(this.userId)){
            holder.postDeleteBtn.setVisibility(View.VISIBLE);
            holder.postDeleteBtn.setEnabled(true);
            holder.postEditBtn.setVisibility(View.VISIBLE);
            holder.postEditBtn.setEnabled(true);
        }else{
            holder.postDeleteBtn.setVisibility(View.GONE);
            holder.postDeleteBtn.setEnabled(false);
            holder.postEditBtn.setVisibility(View.GONE);
            holder.postEditBtn.setEnabled(false);
        }
        PostFirebase.getInstance().islikePost(currentPost.postId,userId,(like)->{
            if(like ==null){

            }
        });
    }

    @Override
    public int getItemCount() {
        return Posts.size();
    }

    public interface OnItemClickListener {
        void onDeleteClick(View v, int position);
        void onEditClick(View v, int position);
        //void onLikeClick(View v, int position);


    }

    public static class PostHolder extends RecyclerView.ViewHolder {

        public CircleImageView postUserImage;
        public TextView postUsernameView;
        public TextView postDateView;
        public AppCompatImageView defaultImage;
        public TextView postDescView;
        public TextView descUsername;
        public Button postDeleteBtn;
        public String postUid;

        private ImageView postImageView;

        private ImageView postLikeBtn;
        private TextView postLikeCount;
        private Button postEditBtn;
        public ProgressBar progressBar;


        public PostHolder(View itemView) {
            super(itemView);
            postUserImage = itemView.findViewById(R.id.post_user_image);
            postImageView = itemView.findViewById(R.id.post_image);
            postUsernameView = itemView.findViewById(R.id.post_username);
            postDateView = itemView.findViewById(R.id.post_date);
            defaultImage = itemView.findViewById(R.id.new_post_imagef);
            postDescView = itemView.findViewById(R.id.post_desc);
            descUsername = itemView.findViewById(R.id.username);
            postDeleteBtn = itemView.findViewById(R.id.delete_btn);
            postEditBtn = itemView.findViewById(R.id.edit_post_btn);
            progressBar = itemView.findViewById(R.id.HomeProgressBar);
//            postLikeBtn=itemView.findViewById(R.id.post_like_btn);
//            postLikeCount = itemView.findViewById(R.id.post_like_count);
            postDeleteBtn.setOnClickListener(v -> onItemClickListener.onDeleteClick(v, getAdapterPosition()));
            postEditBtn.setOnClickListener(v->onItemClickListener.onEditClick(v, getAdapterPosition()));
          //  postLikeBtn.setOnClickListener(v->onItemClickListener.onLikeClick(v,getAdapterPosition()));
        }
    }


}

