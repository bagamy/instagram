package com.example.instagram.ViewModel;

import android.net.Uri;

import com.example.instagram.Model.Entities.User;
import com.example.instagram.Repositories.UserRepository;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;

public class UserViewModel  extends ViewModel {
    public void saveUser(User user, Uri image) {
        UserRepository.instance.saveUser(user,image,(e)->{},(o)->{});
    }
}
