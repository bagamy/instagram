package com.example.instagram.ViewModel;

import android.app.Application;
import android.net.Uri;

import com.example.instagram.Model.UI.UIPost;
import com.example.instagram.PostRecyclerAdapter;
import com.example.instagram.Repositories.PostRepository;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PostViewModel extends ViewModel {
    public LiveData<List<UIPost>> posts;
    public MutableLiveData<UIPost> selectedPost;
    public LiveData<List<UIPost>> postsForProfile;
    private boolean isPostsBinded = false;
    private boolean isProfileBinded = false;
    public MutableLiveData<Boolean> profileBusy;
    public MutableLiveData<Boolean> feedBusy;

    public PostViewModel() {
        super();
        profileBusy = new MutableLiveData<>();
        feedBusy = new MutableLiveData<>();
        selectedPost = new MutableLiveData<>();

        profileBusy.setValue(false);
        feedBusy.setValue(false);
    }

    public LiveData<List<UIPost>> getAllPosts() {
        if (!isPostsBinded) {
            isPostsBinded = true;
            feedBusy.setValue(true);
            posts = PostRepository.instance.getAllPosts();
        }
        return posts;
    }

    public LiveData<List<UIPost>> getAllProfilePosts(String uid) {
        if (!isProfileBinded) {
            isProfileBinded = true;
            profileBusy.setValue(true);
            postsForProfile = PostRepository.instance.getAllPostsForProfile(uid);
        }
        return postsForProfile;
    }


    public void deletePost(String postUid) {
        PostRepository.instance.deletePost(postUid);
        updateBusy();
    }

    public MutableLiveData<UIPost> getSelectedPost(){
        return this.selectedPost;
    }

    public void selectPost(UIPost post){
        this.selectedPost.setValue(post);
    }

    public void updatePost(UIPost currPost,
                             String newDesc,
                             OnSuccessListener onSuccessListener,
                             OnFailureListener onFailureListener){
        PostRepository.instance.updatePost(currPost.postId,newDesc,onSuccessListener,onFailureListener);
    }

    public void likePost(UIPost currPost,
                         String userId,
                         OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener){
        PostRepository.instance.likePost(currPost.postId,userId,onSuccessListener,onFailureListener);

    }

    public void dislikePost(UIPost currPost,
                         String userId,
                         OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener){
        PostRepository.instance.dislikePost(currPost.postId,userId,onSuccessListener,onFailureListener);

    }

    public boolean slikePost(UIPost currPost,
                            String userId,
                            OnSuccessListener onSuccessListener,
                            OnFailureListener onFailureListener){
        PostRepository.instance.dislikePost(currPost.postId,userId,onSuccessListener,onFailureListener);
return true;
    }


    private void updateBusy() {
        profileBusy.setValue(true);
        feedBusy.setValue(true);
    }


}
