package com.example.instagram.Fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.instagram.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {

    private BottomNavigationView mainBottomNav;
    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);
    //mainBottomNav = (BottomNavigationView)view.findViewById(R.id.mainBtmNav);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        NavController navController = Navigation.findNavController(getActivity(),R.id.nav_host_fragment );
        if (auth.getCurrentUser() == null) {
            if (navController.getCurrentDestination().getId() != R.id.loginFragment) {
               // mainBottomNav.setVisibility(View.GONE);
                navController.navigate(R.id.action_mainFragment_to_loginFragment);
            }
        } else {
            if (navController.getCurrentDestination().getId() != R.id.homeFragment) {
                navController.navigate(R.id.homeFragment);
            }
        }

        return view;
    }

}
