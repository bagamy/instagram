package com.example.instagram.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.instagram.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private EditText etxtRegEmailField;
    private EditText etxtRegPassField;
    private EditText etxtRegPassConfirmationField;
    private Button btnRegister;
    private Button reg_login_btn;
    private ProgressBar RegProgress;
    private Toolbar mainToolbar;
    private BottomNavigationView navigationView;
private NavController navController;
    private FirebaseAuth mAuth;
    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_register, container, false);

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        mAuth = FirebaseAuth.getInstance();
        navigationView = getActivity().findViewById(R.id.mainBtmNav);
        navigationView.setVisibility(View.GONE);
        mainToolbar = getActivity().findViewById(R.id.main_toolbar);
        mainToolbar.setVisibility(View.GONE);
        etxtRegEmailField = (EditText) view.findViewById(R.id.reg_email);
        etxtRegPassField = (EditText) view.findViewById(R.id.reg_pass);
        etxtRegPassConfirmationField = (EditText) view.findViewById(R.id.reg_conf_pass);
        btnRegister = (Button) view.findViewById(R.id.reg_btn);
        reg_login_btn = (Button) view.findViewById(R.id.reg_login_btn);
        RegProgress = (ProgressBar) view.findViewById(R.id.regProgressBar);

        reg_login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.popBackStack();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strEmail = etxtRegEmailField.getText().toString();
                String strPass = etxtRegPassField.getText().toString();
                String strConfPass = etxtRegPassConfirmationField.getText().toString();

                if(emailNotEmpty(strEmail) && PassNotEmpty(strPass) && PassConfNotEmpty(strConfPass) && PassConfEq(strConfPass, strPass)){
                    RegProgress.setVisibility(View.VISIBLE);
                    mAuth.createUserWithEmailAndPassword(strEmail, strPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()) {
                                //todo: add mesage
                                NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
                                navController.navigate(R.id.action_registerFragment_to_setupActivityFragment);
                            } else {

                                String e = task.getException().getMessage();
                                Toast.makeText(getActivity(), "Error: " + e, Toast.LENGTH_LONG).show();
                            }
                            RegProgress.setVisibility(View.INVISIBLE);

                        }
                    });
                }
                else {
                    Toast.makeText(getActivity(), "Error: Password done match.", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }

    public boolean emailNotEmpty(String email) {
        if (email.trim().equals("")) {
            Toast.makeText(getActivity(), "Must have mail", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean PassNotEmpty(String pass){
        if (pass.trim().equals("")) {
            Toast.makeText(getActivity(), "Must have pass", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean PassConfNotEmpty(String passConf){
        if (passConf.trim().equals("")) {
            Toast.makeText(getActivity(), "Must have conf", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean PassConfEq(String passConf, String pass){
        if (!passConf.equals(pass)) {
            Toast.makeText(getActivity(), "not Equel", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
