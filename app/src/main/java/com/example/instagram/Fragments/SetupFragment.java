package com.example.instagram.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import de.hdodenhof.circleimageview.CircleImageView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.instagram.Model.Entities.User;
import com.example.instagram.Model.firebase.UserFirebase;
import com.example.instagram.R;
import com.example.instagram.Repositories.UserRepository;
import com.example.instagram.ViewModel.UserViewModel;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class SetupFragment extends Fragment {

    private CircleImageView setupImage;
    private Uri mainImageURI = null;
    private EditText setupName;
    private Button setupButton;
    private ProgressBar setupProgress;
    private String user_id;
    private Toolbar mainToolbar;
    private boolean isNew=false;
    UserViewModel userViewModel;
    private BottomNavigationView navigationView;
    boolean isChanged = false;

    public SetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_setup, container, false);

        mainToolbar = getActivity().findViewById(R.id.main_toolbar);
        mainToolbar.setTitle("Setup Account");
        navigationView = getActivity().findViewById(R.id.mainBtmNav);
        navigationView.setVisibility(View.GONE);
        setupImage = view.findViewById(R.id.setup_image);
        setupProgress = view.findViewById(R.id.setup_progbar);
        setupButton = view.findViewById(R.id.setup_button);
        setupName = view.findViewById(R.id.setupName);
        userViewModel = ViewModelProviders.of(getActivity()).get(UserViewModel.class);

        user_id = FirebaseAuth.getInstance().getCurrentUser().getUid();

        UserFirebase.getUserByUid(user_id, new OnSuccessListener<User>() {
            @Override
            public void onSuccess(User user) {
                if(user != null) {
                    //TODO check if work
                    mainImageURI = Uri.parse(user.getImage());
                    setupName.setText(user.getName());
                    Picasso.get().load(mainImageURI).fit().placeholder(R.drawable.image_placeholder).fit().into(setupImage);
                }else {
                    isNew = true;
                    Toast.makeText(getActivity(), "You need to Initialize User Name and Profile Picture First",Toast.LENGTH_LONG).show();
                }
            }
        });


        setupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(getActivity(), "Permission Denied", Toast.LENGTH_LONG).show();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                    } else {
                        ImagePicker();
                    }
                } else {
                    ImagePicker();
                }
            }

        });

        setupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String userName = setupName.getText().toString();
                final  String image = mainImageURI.toString();
                if(usernameNotEmpty(userName) && PicNotEmpty(image)){
                    if(isNew){
                        saveUser(userName, image);
                    }
                    else {
                        updateUser(userName, image,isChanged);
                    }
                }
            }
        });
        return view;
    }

    private void saveUser(String userName,String image) {
        setupProgress.setVisibility(View.VISIBLE);
        setupButton.setEnabled(false);
        User user = new User();
        user.setName(userName);
        user.setUserId(user_id);


        //user.setImageId(UUID.randomUUID().toString());
        UserRepository.instance.saveUser(user, mainImageURI,(e)->{
            setupProgress.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), "succeed", Toast.LENGTH_SHORT).show();
            setupButton.setEnabled(true);
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_setupActivityFragment_to_homeFragment);
        },(error)->{
            setupProgress.setVisibility(View.INVISIBLE);
            String strError = error.getMessage();
            Toast.makeText(getActivity(), "Image Error: " + strError, Toast.LENGTH_LONG).show();
            setupButton.setEnabled(true);
        } );
        }

    public void updateUser(String userName, String imageURI, boolean isChanged){
        setupProgress.setVisibility(View.VISIBLE);
        setupButton.setEnabled(false);
        UserRepository.instance.updateUser(user_id,userName,imageURI,isChanged,(e)->{
            setupProgress.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), "succeed", Toast.LENGTH_SHORT).show();
            setupButton.setEnabled(true);
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.popBackStack(R.id.homeFragment,false);
        },(error)->{
            setupProgress.setVisibility(View.INVISIBLE);
            String strError = error.getMessage();
            Toast.makeText(getActivity(), "Image Error: " + strError, Toast.LENGTH_LONG).show();
            setupButton.setEnabled(true);
        } );
    }
    public boolean usernameNotEmpty(String userName) {
        if (userName.trim().equals("")) {
            Toast.makeText(getActivity(), "Must have name", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean PicNotEmpty(String imageURI){
        if (imageURI == null ) {
            Toast.makeText(getActivity(), "Muse Have Image", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void ImagePicker() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(getContext(), SetupFragment.this);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainImageURI = result.getUri();
                isChanged = true;
                Picasso.get().load(mainImageURI).fit().placeholder(R.drawable.image_placeholder).fit().into(setupImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
