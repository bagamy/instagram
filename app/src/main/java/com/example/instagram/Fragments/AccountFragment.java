package com.example.instagram.Fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.instagram.MainActivity;
import com.example.instagram.Model.Entities.User;
import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.UI.UIPost;
import com.example.instagram.Model.firebase.UserFirebase;
import com.example.instagram.PostRecyclerAdapter;
import com.example.instagram.R;
import com.example.instagram.Repositories.UserRepository;
import com.example.instagram.ViewModel.PostViewModel;
import com.example.instagram.ViewModel.UserViewModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {

    private List<UIPost> currPosts;
    private PostViewModel postViewModel;
    private String userName;
    private boolean isBinded = false;
    private RecyclerView photosListView;
    private Toolbar mainToolbar;
    private PostRecyclerAdapter postRecyclerAdapter;
    private ProgressBar progressBar;
    private BottomNavigationView navigationView;
    private String userId;
    public AccountFragment() {
        currPosts =null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        postViewModel = ViewModelProviders.of(getActivity()).get(PostViewModel.class);
        bindAdapterToLivedata();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_account, container, false);

        navigationView = getActivity().findViewById(R.id.mainBtmNav);
        navigationView.setVisibility(View.VISIBLE);
        mainToolbar = getActivity().findViewById(R.id.main_toolbar);
        mainToolbar.setVisibility(View.VISIBLE);
        progressBar = view.findViewById(R.id.AccountProgressBar);

        userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        UserFirebase.getUserByUid(userId,(user)->{
            userName = user.getName();
            mainToolbar.setTitle(userName);
        });

        photosListView = view.findViewById(R.id.photos_view);
        photosListView.setHasFixedSize(true);

        photosListView.setLayoutManager(new LinearLayoutManager(getContext()));
        postRecyclerAdapter = new PostRecyclerAdapter(getContext(),
                currPosts,
                userId,
                new PostRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onDeleteClick(View v, int position) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                        alertDialog.setTitle("Delete Post?")
                                .setMessage("Are You Sure You Want to Delete this Post?")
                                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        postViewModel.deletePost(currPosts.get(position).postId);
                                    }
                                });
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }
                    public void onEditClick(View v, int position) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                        alertDialog.setTitle("Edit Post");
                        final EditText input = new EditText(getContext());
                        input.setInputType(InputType.TYPE_CLASS_TEXT);
                        alertDialog.setView(input);
                        alertDialog.setMessage("Put new description")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        postViewModel.updatePost(currPosts.get(position), input.getText().toString(), new OnSuccessListener() {
                                            @Override
                                            public void onSuccess(Object o) {
                                                Toast.makeText(getContext(), "Post Updated", Toast.LENGTH_LONG).show();
                                            }
                                        }, new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            }
                                        });
                                    }
                                });
                        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        alertDialog.show();
                    }


                });

//        postViewModel.feedBusy.observe(this, (isBusy) -> {
//            if (isBusy) {
//                turnOnProgressBar();
//            } else {
//                turnOffProgressBar();
//            }
//        });

//        bindAdapterToLivedata();
        turnOffProgressBar();
        photosListView.setAdapter(postRecyclerAdapter);
        return view;
    }


    private void bindAdapterToLivedata() {
        if (!isBinded) {
            isBinded = true;

            postViewModel.profileBusy.observe(this, (isBusy) -> {
                if (isBusy) {
                    turnOnProgressBar();
                } else {
                    turnOffProgressBar();
                }
            });

            postViewModel.getAllProfilePosts(
                    FirebaseAuth.getInstance().getCurrentUser().getUid()).observe(this, this::updatePosts);
        }
//        turnOnProgressBar();
//        postViewModel.getAllProfilePosts(userId).observe(this, (posts) -> {
//            if (posts.isEmpty()) {
//                photosListView.setVisibility(View.GONE);
//            }
//            else {
//                photosListView.setVisibility(View.VISIBLE);
//
//            }
//            postRecyclerAdapter.Posts = posts;
//            currPosts = posts;
//            postRecyclerAdapter.notifyDataSetChanged();
//            postViewModel.feedBusy.setValue(false);
//            turnOffProgressBar();
//        });
    }

    private void turnOnProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
        photosListView.setVisibility(View.GONE);
    }

    private void turnOffProgressBar() {
        progressBar.setVisibility(View.GONE);
        photosListView.setVisibility(View.VISIBLE);

    }

    private void updatePosts(List<UIPost> posts) {
        if (posts.isEmpty()) {
            photosListView.setVisibility(View.GONE);
            //emptyView.setVisibility(View.VISIBLE);
        }
        else {
            photosListView.setVisibility(View.VISIBLE);
            //emptyView.setVisibility(View.GONE);
        }
        postRecyclerAdapter.Posts = posts;
        currPosts = posts;
        postRecyclerAdapter.notifyDataSetChanged();
        postViewModel.profileBusy.setValue(false);
        turnOffProgressBar();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        postViewModel.profileBusy.removeObservers(this);
        postViewModel.postsForProfile.removeObservers(this);
        isBinded = false;
    }
}
