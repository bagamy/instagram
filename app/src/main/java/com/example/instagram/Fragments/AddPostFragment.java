package com.example.instagram.Fragments;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.instagram.MainActivity;
import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.UI.UIPost;
import com.example.instagram.R;
import com.example.instagram.Repositories.PostRepository;
import com.example.instagram.ViewModel.PostViewModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddPostFragment extends Fragment {

    private static final String TAG = "AddEditPostFragment";
    private static final String APP_NAME = "Instagram";
    private static final int REQUEST_IMAGE_CAPTURE = 0;

    // permission section
    private static final int REQUEST_CAMERA_AND_STORAGE = 0;
    private static final int REQUEST_CURRENT_LOCATION = 1;

    private static String[] CAMERA_AND_STORAGE_PERMISSION = {
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    private Toolbar mainToolbar;
    private ImageView addPostImg;
    private EditText addPostDesc;
    private EditText editPostDesc;
    private Button addPostBtn;
    private ProgressBar addPostPrgBar;
    private Button btnTakePicture;
    private BottomNavigationView navigationView;

    private String type;
    private UIPost currPost;
    private PostViewModel postViewModel;
    private Uri newImageUri=null;
    private String currentUID;


//    private StorageReference storageRef;
//    private FirebaseFirestore fireStore;
//    private FirebaseAuth firebaseAuth;
    public AddPostFragment() {
      postViewModel=null;
    }

    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        postViewModel = ViewModelProviders.of(getActivity()).get(PostViewModel.class);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if (type.equals("edit")) {
//            postViewModel.getSelectedPost().removeObservers(this);
//        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_post, container, false);

        mainToolbar = getActivity().findViewById(R.id.main_toolbar);
        mainToolbar.setTitle("New Post");
        navigationView = getActivity().findViewById(R.id.mainBtmNav);
        navigationView.setVisibility(View.GONE);
        //((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addPostBtn = (Button)view.findViewById(R.id.addPstBtn);
        addPostDesc= (EditText) view.findViewById(R.id.newPostDesc);
        addPostImg=(ImageView) view.findViewById(R.id.new_post_imagef);
        addPostPrgBar=(ProgressBar)view.findViewById(R.id.newPostPrgBar);

        addPostImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker();
            }
        });

        addPostBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String desc = addPostDesc.getText().toString();
               if(DescNotEmpty() && PicNotEmpty()){
                    publishPost();
                    addPostPrgBar.setVisibility(View.VISIBLE);
               }
            }
        });
        return view;
    }

    public void publishPost() {

            addPostPrgBar.setVisibility(View.VISIBLE);
            addPostBtn.setEnabled(false);
            //btnTakePicture.setEnabled(false);
        String newId = UUID.randomUUID().toString();

        PostRepository.instance.savePost(addPostDesc.getText().toString(), newImageUri,newId,(res)->{
            addPostPrgBar.setVisibility(View.INVISIBLE);
            Toast.makeText(getActivity(), "succeed", Toast.LENGTH_SHORT).show();
            NavController navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
            navController.popBackStack(R.id.homeFragment,false);
        },(error)->{
            addPostPrgBar.setVisibility(View.INVISIBLE);
            String strError = error.getMessage();
            Toast.makeText(getActivity(), "Image Error: " + strError, Toast.LENGTH_LONG).show();
        } );
    }

    public boolean DescNotEmpty() {
        if (addPostDesc.getText().toString().trim().equals("")) {
            Toast.makeText(getActivity(), "Must have description", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean PicNotEmpty(){
        if (newImageUri == null ) {
            Toast.makeText(getActivity(), "Muse Have Image", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
    private void ImagePicker() {

        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1)
                .start(getContext(),AddPostFragment.this);
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                newImageUri = result.getUri();
                Picasso.get().load(newImageUri).fit().placeholder(R.drawable.image_placeholder).fit().into(addPostImg);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
