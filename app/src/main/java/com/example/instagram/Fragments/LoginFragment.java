package com.example.instagram.Fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.instagram.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private EditText etxtLoginEmail;
    private EditText etxtLoginPass;
    private Button btnLogin;
    private Button btnLoginReg;
    private ProgressBar loginProgress;
    private Toolbar mainToolbar;
    private FirebaseAuth mAuth;
    private BottomNavigationView navigationView;
    private NavController navController;

    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navigationView = getActivity().findViewById(R.id.mainBtmNav);
        navigationView.setVisibility(View.GONE);
        mainToolbar = getActivity().findViewById(R.id.main_toolbar);
        mainToolbar.setVisibility(View.GONE);
        mAuth = FirebaseAuth.getInstance();
        etxtLoginEmail = (EditText) view.findViewById(R.id.login_email);
        etxtLoginPass = (EditText) view.findViewById(R.id.login_pass);
        btnLogin = (Button) view.findViewById(R.id.login_btn);
        btnLoginReg = (Button) view.findViewById(R.id.login_reg);
        loginProgress = (ProgressBar) view.findViewById(R.id.loginProgressBar);

        btnLoginReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_loginFragment_to_registerFragment);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginProgress.setVisibility(View.VISIBLE);
                String strLoginEmail = etxtLoginEmail.getText().toString();
                String strLoginPass = etxtLoginPass.getText().toString();

                if(!TextUtils.isEmpty(strLoginEmail) && !TextUtils.isEmpty(strLoginPass))
                {
                    loginProgress.setVisibility(View.VISIBLE);

                    mAuth.signInWithEmailAndPassword(strLoginEmail,strLoginPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){
                                navController.navigate(R.id.action_loginFragment_to_homeFragment);
                            }
                            else{
                                // TODO: toast
                                String e = task.getException().getMessage();
                                Toast.makeText(getActivity(), "Error: " + e, Toast.LENGTH_LONG).show();
                            }

                            loginProgress.setVisibility(View.INVISIBLE);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            e.getCause();
                        }
                    });
                }
                else{
                    //todo:add toast
                }
            }
        });
        return view;
    }

}
