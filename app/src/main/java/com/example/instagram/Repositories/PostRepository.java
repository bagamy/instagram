package com.example.instagram.Repositories;

import android.graphics.Path;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Pair;

import com.example.instagram.Model.AppLocalDB;
import com.example.instagram.Model.Entities.Post;
import com.example.instagram.Model.Entities.User;
import com.example.instagram.Model.UI.UIPost;
import com.example.instagram.Model.firebase.PostFirebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class PostRepository {
    public static final PostRepository instance = new PostRepository();
    public MutableLiveData<List<UIPost>> posts;
    public MutableLiveData<List<UIPost>> postsForUser;

    PostRepository() {
        posts = new MutableLiveData<>();
        postsForUser = new MutableLiveData<>();
    }

    public LiveData<List<UIPost>> getAllPosts() {
        AsyncTask task = new GetAllPostsTask();
        task.execute(new String[]{});

        return this.posts;
    }

    public LiveData<List<UIPost>> getAllPostsForProfile(String uid) {
        synchronized (this) {
            AsyncTask task = new GetAllPostsForProfileTask();
            task.execute(new String[]{uid});
        }

        return this.postsForUser;
    }

    public void savePost(String description,
                                  Uri image,
                                  String imageId,
                                  OnSuccessListener onSuccessListener,
                                  OnFailureListener onFailureListener) {
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            Post post = new Post();
            post.setPostId(imageId);
            post.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());
            post.setTimestamp(new Date().getTime());
            post.setDescription(description);
            PostFirebase.getInstance().saveImage(image,imageId, (newImageUrl) -> {
                post.setImage_uri(newImageUrl.toString());
                PostFirebase.savePost(post, onSuccessListener, onFailureListener);
            }, onFailureListener);
            return null;
        });

    }

    public void updatePost(String postId,
                             String newDesc,
                             OnSuccessListener onSuccessListener,
                             OnFailureListener onFailureListener) {
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {

            PostFirebase.getInstance().updatePost(postId, newDesc, onSuccessListener, onFailureListener);

            return null;
        });
    }

    public void likePost(String postId,String userId, OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener){
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            PostFirebase.getInstance().likePost(postId,userId,onSuccessListener,onFailureListener);
        return null;
        });

    }

    public void dislikePost(String postId,String userId, OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener){
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            PostFirebase.getInstance().dislikePost(postId,userId,onSuccessListener,onFailureListener);
            return null;
        });

    }


    private List<UIPost> makePostsForList(List<Post> posts, List<User> users) {
        List<UIPost> result = new ArrayList<>();

        List<UIPost> finalResult = result;

        posts.stream().forEach((post) -> {
            Optional<User> optUser = users.stream().filter(
                    (user1) -> user1.getUserId().equals(post.getUserId()))
                    .findAny();

            if (!optUser.isPresent()) {
                return;
            }

            User user = optUser.get();

            UIPost uiPost = new UIPost();
            uiPost.postId = post.getPostId();
            uiPost.userName = user.getName();
            uiPost.timestamp = post.getTimestamp();
            uiPost.description = post.getDescription();
            uiPost.userId = post.getUserId();
            uiPost.imageUri = post.getImage_uri();
            uiPost.postImageRequestCreator = Picasso.get().load(post.getImage_uri()).resize(500, 500);
            uiPost.userProfileRequestCreator = Picasso.get().load(user.getImage()).resize(100,100);

            finalResult.add(uiPost);
        });

        result = finalResult.stream().sorted(Comparator.comparing(UIPost::getTimestamp).reversed()).collect(Collectors.toList());
        return result;
    }

    private List<UIPost> makePostsForList(List<Post> posts, User user) {
        List<User> userList = new ArrayList<>();
        userList.add(user);
        return makePostsForList(posts, userList);
    }

    @WorkerThread
    private class GetAllPostsTask extends AsyncTask<String, String, List<UIPost>> {

        @Override
        protected List<UIPost> doInBackground(String... strings) {
            try {
                final TaskCompletionSource<Pair<List<Post>, List<User>>> source = new TaskCompletionSource<>();

                PostFirebase.getInstance().getAllCollections((pair) -> {
                    System.out.println("test");
                    Tasks.call(Executors.newSingleThreadExecutor(), () -> {
                        Pair<List<Post>, List<User>> tuple = (Pair<List<Post>, List<User>>) pair;
                        List<Post> postsList = tuple.first;
                        List<User> users = tuple.second;
                        List<UIPost> result = makePostsForList(postsList, users);

                        posts.postValue(result);

                        AppLocalDB.db.postDao().deleteAllPosts();
                        AppLocalDB.db.usersDao().deleteAllUsers();

                        AppLocalDB.db.usersDao().insertAllUsers(users.toArray(new User[0]));
                        AppLocalDB.db.postDao().insertAllPosts(postsList.toArray(new Post[0]));
                        return true;
                    });
                });

                List<Post> postsFromDb = AppLocalDB.db.postDao().getAllPosts();
                List<User> usersFromDb = AppLocalDB.db.usersDao().getAllUsers();

            if (postsFromDb.size() != 0) {
                List<UIPost> a = makePostsForList(postsFromDb, usersFromDb);
                posts.postValue(a);
            }

            } catch(Exception ex) {
                System.out.println(ex.getMessage());
            }


            return null;
        }
    }

    private class GetAllPostsForProfileTask extends AsyncTask<String, String, List<UIPost>> {
        @Override
        protected List<UIPost> doInBackground(String... uid) {
            final TaskCompletionSource<Pair<List<Post>, User>> source = new TaskCompletionSource<>();

            PostFirebase.getInstance().getAllCollectionsForProfile(uid[0], (pair) -> {
                Tasks.call(Executors.newSingleThreadExecutor(), () -> {
                    Pair<List<Post>, User> tuple = (Pair<List<Post>, User>) pair;
                    List<Post> posts = tuple.first;
                    User user = tuple.second;
                    List<UIPost> result = makePostsForList(posts, user);
                    postsForUser.postValue(result);

                    AppLocalDB.db.usersDao().insertAllUsers(user);
                    AppLocalDB.db.postDao().insertAllPosts(posts.toArray(new Post[0]));

                    return true;
                });
            });

            List<Post> postsFromDb = AppLocalDB.db.postDao().getAllPosts();
            User usersFromDb = AppLocalDB.db.usersDao().getUserById(uid[0]);

            postsForUser.postValue(makePostsForList(postsFromDb, usersFromDb));

            return null;
        }
    }

    public void deletePost(String postUid) {
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            PostFirebase.getInstance().deletePost(postUid);
            AppLocalDB.db.postDao().deletePost(postUid);
            return true;
        });
    }

}
