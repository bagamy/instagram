package com.example.instagram.Repositories;

import android.net.Uri;
import android.provider.Contacts;
import android.widget.Toast;

import com.example.instagram.Model.AppLocalDB;
import com.example.instagram.Model.Entities.User;
import com.example.instagram.Model.firebase.PostFirebase;
import com.example.instagram.Model.firebase.UserFirebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.concurrent.Executors;

public class UserRepository {
    public static final UserRepository instance = new UserRepository();

    public void saveUser(User user, Uri image,OnSuccessListener onSuccessListener,
                         OnFailureListener onFailureListener) {
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {

            PostFirebase.getInstance().saveImage(image, user.getUserId(),(newImage)->{

                user.setImage(newImage.toString());
                UserFirebase.saveUser(user, onSuccessListener, onFailureListener);
                AppLocalDB.db.usersDao().insertUser(user);
            }, onFailureListener);

            return null;
        });
    }


    public void updateUser(String UID,
                           String userName,
                           String newImage,
                           boolean isChanged,
                           OnSuccessListener onSuccessListener,
                           OnFailureListener onFailureListener) {
        Tasks.call(Executors.newSingleThreadExecutor(), () -> {
            if(isChanged) {

                PostFirebase.getInstance().deleteImage(UID, (e) -> {
                    PostFirebase.getInstance().saveImage(Uri.parse(newImage), UID, (newImageURL) -> {
                        UserFirebase.getInstance().updateUser(UID, userName, newImageURL.toString(), onSuccessListener, onFailureListener);
                    }, onFailureListener);
                }, (ex) -> {
                    PostFirebase.getInstance().saveImage(Uri.parse(newImage), UID, (newImageURL) -> {
                        UserFirebase.getInstance().updateUser(UID, userName, newImageURL.toString(), onSuccessListener, onFailureListener);
                    }, onFailureListener);
                });
            }else{
                UserFirebase.getInstance().updateUser(UID, userName, newImage, onSuccessListener, onFailureListener);
            }
            return null;
        });
    }



}
